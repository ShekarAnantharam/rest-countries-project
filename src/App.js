import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Routes, Route } from "react-router-dom";
import CountryView from "./components/countryview";
import './App.css';
import Countries from './components/Countries'
import CountriesList from "./components/API/api";
import * as API from "./components/API/api"

class Home extends React.Component {

  constructor(props){
    super(props)
    this.state={
        data:[],
        searchData:[]
    }
}
getData=async ()=>{
  const data = await API.getData();
  this.setState({data:data, searchData:data})
}
handleSearch=(value)=>{
let searchData=this.state.data.filter(country => {
  if (value==""){
    return country
  }
  else if(country.name.common.toLowerCase().includes(value.toLowerCase())){
    return country
  }

}) 
this.setState({searchData})
}
handleFilter=(value)=>{
  let searchData=this.state.data.filter(country => {
    if (value=="Filter by Region"){
      return country
    }
    else if(country.region.toLowerCase()==value.toLowerCase()){
      return country
    }
  
  }) 
  this.setState({searchData})
  }

componentDidMount(){
  this.getData();
}
  render() { 
   
    return (
     <Router>

        <div className="App">
            
        <Routes>

            <Route  path="/" element={<Countries countries = {this.state.searchData} 
            onSearch={this.handleSearch} onFilter={this.handleFilter}/>}/>
            <Route />
            <Route path="/name/:id" element={<CountryView/>}/> 
            
        </Routes>

        </div>

    </Router>
    );
  }
}
 
export default Home;
